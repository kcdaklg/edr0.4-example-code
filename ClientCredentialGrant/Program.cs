﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using EdrServiceClient.gov.wa.courts.api.edr;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientCredentialGrant
{
    class Program
    {
        static Uri _tokenPath = new Uri("https://authp.courts.wa.gov/OAuth/Token");
        static readonly Uri _serviceUri = new Uri("https://api-qa.courts.wa.gov/EdrDataServices/AocEdrDataService.svc/");
        static readonly string _dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName);

        static WebServerClient _webServerClient;
        static string _accessToken;

        static void Main(string[] args)
        {
            // Currently, any client ID/secret will work.
            Util.Spinner("Initialize Web Server Client", () =>
            {
                InitializeWebServerClient("ClientID", "ClientSecret");
            });

            Util.Spinner("Requesting Token", () =>
            {
                RequestToken();
            });
            Console.WriteLine();

            Console.WriteLine("Access Token: {0}", _accessToken);
            Console.WriteLine();

            var rights = Util.Spinner("Access Protected Resource", () =>
            {
                return AccessProtectedResource();
            });
            Console.WriteLine();
            Console.WriteLine("GetSecurityRights:");
            foreach (var right in rights)
            {
                Console.WriteLine("\t{0}", right);
            }
            Console.WriteLine();

            Console.Write("Press any key to continue.");
            Console.ReadKey();
        }

        private static void InitializeWebServerClient(string clientId, string clientSecret)
        {
            var authorizationServer = new AuthorizationServerDescription
            {
                TokenEndpoint = _tokenPath
            };
            _webServerClient = new WebServerClient(authorizationServer, clientId, clientSecret);
        }

        private static void RequestToken()
        {
            try
            {
                var state = _webServerClient.GetClientAccessToken();
                _accessToken = state.AccessToken;
            }
            catch (ProtocolException ex)
            {
                var webException = ex.InnerException as WebException;
                if (webException != null && webException.Response != null)
                {
                    var responseStream = webException.Response.GetResponseStream() as MemoryStream;
                    if (responseStream != null)
                    {
                        Console.Error.WriteLine(Encoding.UTF8.GetString(responseStream.ToArray()));
                    }
                }
                throw ex;
            }
        }

        private static IEnumerable<string> AccessProtectedResource()
        {
            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.SendingRequest2 += (o, e) => e.RequestMessage.SetHeader("Authorization", "Bearer " + _accessToken);
            return edr.Execute<string>(new Uri(edr.BaseUri, "GetSecurityRights"), "GET", false);
        }
    }
}
