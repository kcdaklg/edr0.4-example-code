﻿using EdrServiceClient.gov.wa.courts.api.edr;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;

namespace ReferenceExample
{
    class Program
    {
        static readonly Uri _serviceUri = new Uri("https://api-qa.courts.wa.gov/EdrDataServices/AocEdrDataService.svc/");
        static readonly string _dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName);
        static Random _random = new Random();

        static void Main(string[] args)
        {
            Util.Spinner("Initialize", () =>
            {
                // Currently you can add standard references but your access will be removed after AOC defines their standards.
                InitializeEdrReferenceData();
                InitializeOrganizationReferenceData();
            });


            // load EDR service context
            var edr = new AocEdrDataServiceContext(_serviceUri);
            // set merge options
            edr.MergeOption = MergeOption.OverwriteChanges;

            // this content would come from your local database.
            var actorSourceKey = _random.Next().ToString();
            var firstName = _random.Next().ToString();
            var lastName = _random.Next().ToString();

            #region add actor and actor hair color

            #region get data source
            var dataSource = edr.GetDataSource(_dataSourceCodeValue, true);
            #endregion

            #region get ownership (Temporary!!!)
            var ownership = edr.GetSecurityResourceOwner(dataSource, true);
            #endregion

            // new person
            var person1 = new Person
            {
                Name = string.Format("{0}, {1}", lastName, firstName)
            };
            edr.AddToActors(person1);
            edr.SetLink(person1, "DataSource", dataSource);
            edr.SetLink(person1, "ResourceOwner", ownership); // Temporary

            // new person name
            var personName = new PersonName
            {
                FirstName = firstName,
                LastName = lastName
            };
            edr.AddToPersonNames(personName);
            edr.SetLink(personName, "DataSource", dataSource);
            edr.SetLink(personName, "Person", person1);

            // hair color
            var personHairColor = new HairColor();
            edr.AddToHairColors(personHairColor);
            edr.SetLink(personHairColor, "DataSource", dataSource);
            edr.SetLink(personHairColor, "Person", person1);
            edr.SetLink(personHairColor, "SourceReference", edr.HairColorSourceReferences.Where(x => x.DataSource.CodeValue == _dataSourceCodeValue && x.CodeValue == "BLK").First());

            // key map
            var actorKeyMap = new ActorKeyMap { SourceKey = actorSourceKey };
            edr.AddToActorKeyMaps(actorKeyMap);
            edr.SetLink(actorKeyMap, "DataSource", dataSource);
            edr.SetLink(actorKeyMap, "Actor", person1);

            edr.SaveChanges(withSpinner: true);
            Console.WriteLine();
            #endregion

            #region query for actor using local actor key
            // query for the actor name using KC actor key
            var person2 = Util.Spinner("Query for person and expand into SourceReference", () =>
            {
                return (Person)(from akm in edr.ActorKeyMaps
                              .Expand("Actor/Aoc.Edr.DataContext.Person/HairColors/SourceReference")
                                where akm.SourceKey == actorSourceKey
                                select akm).First().Actor;
            });
            Console.WriteLine("{0}, {1}, {2}", actorSourceKey, person2.Name, person2.HairColors.First().SourceReference.CodeValue);
            #endregion

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        #region Initialize Reference Data

        static void InitializeOrganizationReferenceData()
        {
            var personHairColorSourceReferencesCodeValues = new[] { "BLK", "BRN" };

            var edr = new AocEdrDataServiceContext(_serviceUri);

            #region data source
            var dataSource = edr.GetDataSource(_dataSourceCodeValue, false);
            #endregion

            #region person hair color source references
            var references = edr.HairColorSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in personHairColorSourceReferencesCodeValues)
            {
                var reference = references.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "A Description"
                    };
                    edr.AddToHairColorSourceReferences(reference);
                    edr.SetLink(reference, "DataSource", dataSource);
                    var sr = edr.HairColorStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).First();
                    edr.SetLink(reference, "StandardReference", sr);
                    edr.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
        }

        static void InitializeEdrReferenceData()
        {
            var personHairColorStandardReferenceCodeValues = new[] { "EDR-BLK", "EDR-BRN" };

            #region person hair color standard references
            var edr = new AocEdrDataServiceContext(_serviceUri);
            foreach (var codeValue in personHairColorStandardReferenceCodeValues)
            {
                var reference = edr.HairColorStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "A Description",
                    };
                    edr.AddToHairColorStandardReferences(reference);
                    edr.SaveChanges();
                }
            }
            #endregion
        }
        #endregion
    }
}
