﻿using EdrServiceClient.gov.wa.courts.api.edr;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Services.Client.Async;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace AsynchronousExample
{
    class Program
    {
        static readonly Uri _serviceUri = new Uri("https://api-qa.courts.wa.gov/EdrDataServices/AocEdrDataService.svc/");
        static readonly string _dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName);
        static readonly Random _random = new Random();
        static readonly DateTime _beginDate = DateTime.Now;

        static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        static async Task MainAsync()
        {
            // The number EDR service that can be invoked concurrently.
            //     You should play with this number. It can go up or down.
            var concurrency = 11; // <= https://goo.gl/IFQqZP
            // The number of people to insert.
            var totalPersonInserts = 1000;

            #region Initialize
            var edr = new AocEdrDataServiceContext(_serviceUri);
            var pool = Util.Spinner<ObjectPool<PoolItem>>("Initialize", () =>
            {
                var dataSource = edr.GetDataSource(_dataSourceCodeValue);
                var ownership = edr.GetSecurityResourceOwner(dataSource);
                return new ObjectPool<PoolItem>(concurrency, () =>
                {
                    var poolItem = new PoolItem
                    {
                        DataServiceContext = new AocEdrDataServiceContext(_serviceUri),
                        DataSource = new DataSource { Key = dataSource.Key },
                        SecurityResourceOwner = new SecurityResourceOwner { Key = ownership.Key }
                    };
                    poolItem.DataServiceContext.AttachTo("DataSources", poolItem.DataSource);
                    poolItem.DataServiceContext.AttachTo("SecurityResourceOwners", poolItem.SecurityResourceOwner);
                    return poolItem;
                });
            });
            Console.WriteLine();
            #endregion

            #region Add People
            var watch = Stopwatch.StartNew();
            await Util.SpinnerAsync(string.Format("Add {0} People", totalPersonInserts), async () =>
            {
                await AddPeople(pool, totalPersonInserts);
                await FlushPool(pool);
            });
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Elapsed Seconds: {0}", (int)(elapsedMs * .001));
            Console.WriteLine();
            #endregion

            #region Query People
            var query = (DataServiceQuery<Actor>)edr.Actors.Where(x => x.BeginDate == _beginDate);
            Console.WriteLine("Query People\n  {0}", query.RequestUri.Query);
            var actors = Util.Spinner<IEnumerable<Actor>>("", () =>
            {
                return query.ToArray();
            });
            Console.WriteLine("Total people found: {0}", actors.Count());
            Console.WriteLine();
            #endregion

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        /// <summary>
        /// Adds a collection of people to the actor entity set.
        /// </summary>
        /// <param name="pool">The Data Service Context Pool</param>
        /// <param name="count">The number of people to add.</param>
        /// <returns></returns>
        static async Task AddPeople(ObjectPool<PoolItem> pool, int count)
        {
            var tasks = new List<Task>();
            for (int i = 0; i < count; i++)
            {
                var task = pool.Invoke(AddPerson);
                tasks.Add(task);
            }
            await Task.WhenAll(tasks.ToArray());
        }

        /// <summary>
        /// Add a single person to the actor entity set.
        /// </summary>
        /// <param name="pool">The Data Service Context Pool</param>
        /// <returns></returns>
        static async Task AddPerson(PoolItem poolItem)
        {
            var person = new Person
            {
                // BeginDate will be used when querying our example inserts.
                BeginDate = _beginDate,
                KeyMaps = {
                        new ActorKeyMap
                        {
                            SourceKey = Guid.NewGuid().ToString()
                        }
                    },
                Name = string.Format("{0}, {1}", _random.Next(), _random.Next())
            };

            // get context
            var edr = poolItem.DataServiceContext;

            /*
             *  3 = The total number of multipart body parts defined below. You
             *      must do all 3 because the link between ActorKeyMap and
             *      Actor must always be in the same batch. 
             * 50 = The total number of change set items currently allowed by
             *      the EDR service.
             */
            if (poolItem.BufferCount + 3 > 50)
            {
                // save items in buffer
                await edr.SaveChangesAsync(SaveChangesOptions.Batch);
                // reset buffer count.
                poolItem.BufferCount = 0;
            }

            #region 1) Single multipart body part posting an Actor entity, DataSource and, SecurityResourceOwner links
            edr.AddToActors(person);
            edr.SetLink(person, "DataSource", poolItem.DataSource);
            edr.SetLink(person, "ResourceOwner", poolItem.SecurityResourceOwner);
            #endregion
            poolItem.BufferCount++;

            #region 2) Single multipart body part posting an ActorKeyMap entity and DataSource link
            var keyMap = person.KeyMaps.Single();
            edr.AddToActorKeyMaps(keyMap);
            edr.SetLink(keyMap, "DataSource", poolItem.DataSource);
            #endregion
            poolItem.BufferCount++;

            #region 3) Single multipart body part putting a link between KeyMap entity and NEW Person entity.
            edr.SetLink(keyMap, "Actor", person);
            #endregion
            poolItem.BufferCount++;
        }

        /// <summary>
        /// Saves any pending changes in each of the data source contexts
        /// </summary>
        /// <param name="pool">The Data Service Context Pool</param>
        /// <returns></returns>
        static async Task FlushPool(ObjectPool<PoolItem> pool)
        {
            var tasks = new List<Task>();
            foreach (var pi in pool.ToArray())
            {
                var task = pi.DataServiceContext.SaveChangesAsync(SaveChangesOptions.Batch);
                tasks.Add(task);
            }
            await Task.WhenAll(tasks.ToArray());
        }
    }

    /// <summary>
    /// Instance of this class will be placed in the object pool. You can put
    /// anything in this class that needs to be scoped together.
    /// </summary>
    class PoolItem
    {
        /// <summary>
        /// The data service context (aka. The EDR service).
        /// </summary>
        public AocEdrDataServiceContext DataServiceContext { get; set; }

        /// <summary>
        ///  Used with any entity requiring DataSource
        /// </summary>
        public DataSource DataSource { get; set; }

        /// <summary>
        /// Used with any entity requiring SecurityResourceOwner (Temporary!!!)
        /// </summary>
        public SecurityResourceOwner SecurityResourceOwner { get; set; }

        /// <summary>
        /// The total number of entities/links that have been added or
        /// modified.
        /// </summary>
        public int BufferCount { get; set; }
    }
}
