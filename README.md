# EDR Examples #

A collection of .NET projects focused on demonstrating different ways to interact with the EDR.

### What is this repository for? ###

* Example code for interacting with EDR
* Previewing new features

### How do I get set up? ###

* Open visual studio
* Select '***Team->Connect to Team Foundation Server...***' from the Visual Studio menu. 
* Expand the '***Local Git Repositories***' section.
* Select the '***Clone***' drop down.
* Enter '***https://bitbucket.org/kcdaklg/edr0.4-example-code***' in the select (1st) field.
* Click the '***Clone***' button.
* Enter your '***User name***' and '***Password***' and click '***OK***'.
	* The *user name* and *password* are defined when users create an account with BitBucket.
* Double click the newly added Git repository labeled '***edr-examples***'.
* Under the '***Solutions***' section, double click the solution marked '***EDR-Integration-Examples.sln***'.

### How do I run an Example? ###
* Select a example project.
	* e.g. '***CRUDExample***' project.
* Select '***Debug->Start Debugging***' (`F5`) from the Visual Studio menu.

### Contribution guidelines ###

* Currently we are not accepting contributions. If you wish to contribute, let us know.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact