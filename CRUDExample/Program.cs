﻿using EdrServiceClient.gov.wa.courts.api.edr;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CRUDExample
{
    public class MyPerson
    {
        public string actorSourceKey { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Race { get; set; }
        public string DriverLicNum { get; set; }
        public string DriverLicState { get; set; }
        public DateTime DriverLicExpDate { get; set; }
        public Int16 height { get; set; }
        public Int16 weight { get; set; }
        public string EyeColor { get; set; }
        public string HairColor { get; set; }
        public string Language { get; set; }
        public List<MyPerson> AkaPerson { get; set; }
    }
    class Program
    {
        static readonly Uri _serviceUri = new Uri("https://api-qa.courts.wa.gov/EdrDataServices/AocEdrDataService.svc/");
        static readonly string _dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName);
        //static Random _random = new Random();
        #region . Test Data
        static MyPerson testPerson = new MyPerson
        {
            actorSourceKey = "KCDC9991",
            firstName = "Penelope",
            lastName = "Rodriguez",
            middleName = "Ava",
            DateOfBirth = Convert.ToDateTime("12/12/1990"),
            Gender = "F",
            Race = "W",
            DriverLicNum = "RODRIPA542BC",
            DriverLicState = "WA",
            DriverLicExpDate = Convert.ToDateTime("12/12/2018"),
            height = 62,
            weight = 118,
            EyeColor = "BRN",
            HairColor = "BLK",
            Language = "SPN"
        };
        static MyPerson testPerson1 = new MyPerson
        {
            actorSourceKey = "KCDC9990",
            firstName = "Johnson",
            lastName = "Rodriguez",
            middleName = "Adam",
            DateOfBirth = Convert.ToDateTime("12/01/1980"),
            Gender = "M",
            Race = "W",
            DriverLicNum = "RODRIJS123BC",
            DriverLicState = "WA",
            DriverLicExpDate = Convert.ToDateTime("12/01/2018"),
            height = 68,
            weight = 168,
            EyeColor = "BRN",
            HairColor = "BLK",
            Language = "SPN"
        };
        static MyPerson testPerson2 = new MyPerson
        {
            actorSourceKey = "KCDC9992",
            firstName = "John",
            lastName = "Rodriguez",
            middleName = "Aaron",
            DateOfBirth = Convert.ToDateTime("05/01/1993"),
            Gender = "M",
            Race = "W",
            DriverLicNum = "RODRIJA789BC",
            DriverLicState = "WA",
            DriverLicExpDate = Convert.ToDateTime("05/01/2018"),
            height = 65,
            weight = 148,
            EyeColor = "BRN",
            HairColor = "BLK",
            Language = "SPN"
        };
        static MyPerson testPerson3 = new MyPerson
        {
            actorSourceKey = "KCDC0005",
            firstName = "Steve",
            lastName = "Jobs",
            middleName = "Aaron",
            DateOfBirth = Convert.ToDateTime("01/01/1950"),
            Gender = "M",
            Race = "W",
            DriverLicNum = "JOBS**789*S",
            DriverLicState = "WA",
            DriverLicExpDate = Convert.ToDateTime("01/01/2018"),
            height = 69,
            weight = 148,
            EyeColor = "BRN",
            HairColor = "BRN",
            Language = "ENG"
        };
        static MyPerson akaPerson = new MyPerson
        {
            actorSourceKey = "KCDC0002",
            firstName = "Penny",
            lastName = "Garcia",
            middleName = "Ava",
            DateOfBirth = Convert.ToDateTime("12/12/1990"),
            Gender = "F",
            Race = "W",
            height = 61,
            weight = 122,
            EyeColor = "BRN",
            HairColor = "BLK",
            Language = "SPN"
        };
        static MyPerson akaPerson1 = new MyPerson
        {
            actorSourceKey = "KCDC0006",
            firstName = "S",
            lastName = "Jobs",
            middleName = "John",
            DateOfBirth = Convert.ToDateTime("01/01/1950"),
            Gender = "M",
            Race = "W",
            height = 69,
            weight = 148,
            EyeColor = "BRN",
            HairColor = "BRN",
            Language = "ENG"
        };
        #endregion
        static void Main(string[] args)
        {
            // do insert
           Insert(testPerson1);
           Insert(testPerson2);
            //link person1 to person1 and make person1 to be primary and person2 to be aka
           AddAlias(testPerson1.actorSourceKey, testPerson2.actorSourceKey);
            // select newly inserted value
          Select(testPerson1.actorSourceKey);
            // update newly inserted value (swap first with last)
            // Update(actorSourceKey, lastName, firstName);
            // select newly updated value
            //Select(actorSourceKey);
            // delete newly updated value
            // Delete(actorSourceKey);
            // select showing that value has been removed.
            //Select(actorSourceKey);

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        static void Insert(MyPerson p)
        {
            Console.WriteLine("Insert: '{0}'.", string.Format("{0}, {1}", p.lastName, p.firstName));

            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.Format.UseJson();
            edr.MergeOption = MergeOption.OverwriteChanges;

            #region get data source
            var dataSource = edr.DataSources
                .Expand("HairColorSourceReferences")
                .Expand("EyeColorSourceReferences")
                .Expand("GenderSourceReferences")
                .Expand("RaceSourceReferences")
                .Expand("LanguageSourceReferences")
                .Expand("ActorIdentitySourceReferences")
                .Expand("ActorEventDateSourceReferences")
                .Where(x => x.CodeValue == _dataSourceCodeValue).First();
            #endregion

            #region get ownership (Temporary!!!)
            var ownership = edr.GetSecurityResourceOwner(dataSource);
            #endregion

            #region insert new person

            var person = new Person
            {
                Name = string.Format("{0}, {1}", p.lastName, p.firstName)
            };
            edr.AddToActors(person);
            edr.SetLink(person, "DataSource", dataSource);
            edr.SetLink(person, "ResourceOwner", ownership); // Temporary
            #region Driver License
            if (p.DriverLicNum != null)
            {
                var personIdentity = new ActorIdentity
                {
                    DataSource = dataSource,
                    Value = p.DriverLicNum,
                    ExpireDate = p.DriverLicExpDate,
                    AuthorizingOrganization = p.DriverLicState
                };
                edr.AddToActorIdentities(personIdentity);
                edr.SetLink(personIdentity, "DataSource", dataSource);
                edr.SetLink(personIdentity, "ResourceOwner", ownership);
                edr.SetLink(personIdentity, "SourceReference", dataSource.ActorIdentitySourceReferences.Where(x => x.CodeValue == p.DriverLicState).First());
                edr.AddLink(person, "Identities", personIdentity);

                var actorIdentityKeyMap = new ActorIdentityKeyMap
                {
                    DataSource = dataSource,
                    SourceKey = p.actorSourceKey
                };
                edr.AddToActorIdentityKeyMaps(actorIdentityKeyMap);
                edr.SetLink(actorIdentityKeyMap, "Identity", personIdentity);
                edr.SetLink(actorIdentityKeyMap, "DataSource", dataSource);
            }
            #endregion
            #region DOB, Height, Weight
            #region DOB
            var actorEvent = new ActorEvent
            {
                DataSource = dataSource,
                ResourceOwner = ownership
            };
            edr.AddToActorEvents(actorEvent);
            edr.SetLink(actorEvent, "DataSource", dataSource);
            edr.SetLink(actorEvent, "ResourceOwner", ownership);
            edr.SetLink(actorEvent, "Actor", person);
            var actorEventKeyMap = new ActorEventKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToActorEventKeyMaps(actorEventKeyMap);
            edr.SetLink(actorEventKeyMap, "ActorEvent", actorEvent);
            edr.SetLink(actorEventKeyMap, "DataSource", dataSource);
            var actorDOB = new ActorEventDate 
            { 
                DataSource = dataSource,
                Date = p.DateOfBirth
            };
            edr.AddToActorEventDates(actorDOB);
            edr.SetLink(actorDOB, "ActorEvent", actorEvent);
            edr.SetLink(actorDOB, "DataSource", dataSource);
            edr.SetLink(actorDOB, "SourceReference", dataSource.ActorEventDateSourceReferences.Where(x => x.CodeValue == "DOB").First());
            var actorEventDateKeyMap = new ActorEventDateKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToActorEventDateKeyMaps(actorEventDateKeyMap);
            edr.SetLink(actorEventDateKeyMap, "Date", actorDOB);
            edr.SetLink(actorEventDateKeyMap, "DataSource", dataSource);
            #endregion
            #region Height Weight
            var demographic = new Demographic
            {
                //BirthDate = p.DateOfBirth,
                Height = p.height,
                Weight = p.weight
            };
            edr.AddToDemographics(demographic);
            edr.SetLink(demographic, "DataSource", dataSource);
            edr.SetLink(demographic, "Person", person);
           var demographicKeyMap = new DemographicKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToDemographicKeyMaps(demographicKeyMap);
            edr.SetLink(demographicKeyMap, "DataSource", dataSource);
            edr.SetLink(demographicKeyMap, "Demographic", demographic);
            #endregion
            #endregion
            #region hair Color
            var personHairColor = new HairColor();
            edr.AddToHairColors(personHairColor);
            edr.SetLink(personHairColor, "DataSource", dataSource);
            edr.SetLink(personHairColor, "Person", person);
            edr.SetLink(personHairColor, "SourceReference", dataSource.HairColorSourceReferences.Where(x => x.CodeValue == p.HairColor).First());
            var hairColorKeyMap = new HairColorKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToHairColorKeyMaps(hairColorKeyMap);
            edr.SetLink(hairColorKeyMap, "HairColor", personHairColor);
            edr.SetLink(hairColorKeyMap, "DataSource", dataSource);
            #endregion
            #region Gender
            var personGender = new Gender();
            edr.AddToGenders(personGender);
            edr.SetLink(personGender, "DataSource", dataSource);
            edr.SetLink(personGender, "Person", person);
            edr.SetLink(personGender, "SourceReference", dataSource.GenderSourceReferences.Where(x => x.CodeValue == p.Gender).First());
            var genderKeyMap = new GenderKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToGenderKeyMaps(genderKeyMap);
            edr.SetLink(genderKeyMap, "Gender", personGender);
            edr.SetLink(genderKeyMap, "DataSource", dataSource);
            #endregion
            #region Race
            var personRace = new Race();
            edr.AddToRaces(personRace);
            edr.SetLink(personRace, "DataSource", dataSource);
            edr.SetLink(personRace, "Person", person);
            edr.SetLink(personRace, "SourceReference", dataSource.RaceSourceReferences.Where(x => x.CodeValue == p.Race).First());
            var raceKeyMap = new RaceKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToRaceKeyMaps(raceKeyMap);
            edr.SetLink(raceKeyMap, "Race", personRace);
            edr.SetLink(raceKeyMap, "DataSource", dataSource);
            #endregion
            #region Eye Color
            var personEyeColor = new EyeColor();
            edr.AddToEyeColors(personEyeColor);
            edr.SetLink(personEyeColor, "DataSource", dataSource);
            edr.SetLink(personEyeColor, "Person", person);
            edr.SetLink(personEyeColor, "SourceReference", dataSource.EyeColorSourceReferences.Where(x => x.CodeValue == p.EyeColor).First());
            var eyeColorKeyMap = new EyeColorKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToEyeColorKeyMaps(eyeColorKeyMap);
            edr.SetLink(eyeColorKeyMap, "EyeColor", personEyeColor);
            edr.SetLink(eyeColorKeyMap, "DataSource", dataSource);
            #endregion
            #region Language
            var personLanguage = new Language();
            edr.AddToLanguages(personLanguage);
            edr.SetLink(personLanguage, "DataSource", dataSource);
            edr.SetLink(personLanguage, "Actor", person);
            edr.SetLink(personLanguage, "SourceReference", dataSource.LanguageSourceReferences.Where(x => x.CodeValue == p.Language).First());
            var languageKeyMap = new LanguageKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToLanguageKeyMaps(languageKeyMap);
            edr.SetLink(languageKeyMap, "Language", personLanguage);
            edr.SetLink(languageKeyMap, "DataSource", dataSource);
            #endregion
            #region firstname, lastname, middlename
            var personName = new PersonName
            {
                DataSource = dataSource,
                FirstName = p.firstName,
                LastName = p.lastName,
                MiddleName = p.middleName
            };
            edr.AddToPersonNames(personName);
            edr.SetLink(personName, "DataSource", dataSource);
            //edr.SetLink(personName, "Ownership", ownership); // Temporary
            edr.SetLink(personName, "Person", person);
            var personNameKeyMap = new PersonNameKeyMap 
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToPersonNameKeyMaps(personNameKeyMap);
            edr.SetLink(personNameKeyMap, "PersonName", personName);
            edr.SetLink(personNameKeyMap, "DataSource", dataSource);
            #endregion
            var personKeyMap = new ActorKeyMap
            {
                DataSource = dataSource,
                SourceKey = p.actorSourceKey
            };
            edr.AddToActorKeyMaps(personKeyMap);
            edr.SetLink(personKeyMap, "Actor", person);
            edr.SetLink(personKeyMap, "DataSource", dataSource);

            // do insert
            edr.SaveChanges(withSpinner: false);
            #endregion
        }

        static void AddAlias(string primaryActorSourceKey, string akaActorSourceKey)
        {
            Console.WriteLine("Set: '{0} to be {1}'s AKA'.", akaActorSourceKey, primaryActorSourceKey);

            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.Format.UseJson();
            edr.MergeOption = MergeOption.OverwriteChanges;

            #region get data source
            var dataSource = edr.DataSources
                .Expand("ActorAssociationTypeSourceReferences")
                .Expand("ActorAssociationRoleSourceReferences")
                .Where(x => x.CodeValue == _dataSourceCodeValue).First();
            #endregion
            var actorKeyMap = edr.ActorKeyMaps.Expand("Actor").
                Where(x => x.DataSource.CodeValue == dataSource.CodeValue && x.SourceKey == primaryActorSourceKey).SingleOrDefault();
            Actor primaryActor = actorKeyMap.Actor;
            actorKeyMap = edr.ActorKeyMaps.Expand("Actor").
                Where(x => x.DataSource.CodeValue == dataSource.CodeValue && x.SourceKey == akaActorSourceKey).SingleOrDefault();
            Actor akaActor = actorKeyMap.Actor;
            #region Create an Association of AKA
            var actorAssociation = new ActorAssociation();
            edr.AddToActorAssociations(actorAssociation);
            edr.SetLink(actorAssociation, "DataSource", dataSource);
            edr.SetLink(actorAssociation, "TypeSourceReference", dataSource.ActorAssociationTypeSourceReferences.Where(x => x.CodeValue == "AKA").First());
            var actorAssociationKeyMap = new ActorAssociationKeyMap 
            {
                DataSource = dataSource,
                SourceKey = primaryActorSourceKey
            };
            edr.AddToActorAssociationKeyMaps(actorAssociationKeyMap);
            edr.SetLink(actorAssociationKeyMap, "Association", actorAssociation);
            edr.SetLink(actorAssociationKeyMap, "DataSource", dataSource);
            #endregion
            #region Create one primary one aka membership, link these memberships to Association
            var actorAssociationPrimaryMember = new ActorAssociationMember();
            edr.AddToActorAssociationMembers(actorAssociationPrimaryMember);
            edr.SetLink(actorAssociationPrimaryMember, "DataSource", dataSource);
            edr.SetLink(actorAssociationPrimaryMember, "Actor", primaryActor);
            edr.SetLink(actorAssociationPrimaryMember, "Association", actorAssociation);
            edr.SetLink(actorAssociationPrimaryMember, "RoleSourceReference", dataSource.ActorAssociationRoleSourceReferences.Where(x => x.CodeValue == "primary").First());
            var actorAssociationMemberKeyMap = new ActorAssociationMemberKeyMap
            {
                DataSource = dataSource,
                SourceKey = primaryActorSourceKey
            };
            edr.AddToActorAssociationMemberKeyMaps(actorAssociationMemberKeyMap);
            edr.SetLink(actorAssociationMemberKeyMap, "Member", actorAssociationPrimaryMember);
            edr.SetLink(actorAssociationMemberKeyMap, "DataSource", dataSource);

            var actorAssociationAKAMember = new ActorAssociationMember();
            edr.AddToActorAssociationMembers(actorAssociationAKAMember);
            edr.SetLink(actorAssociationAKAMember, "DataSource", dataSource);
            edr.SetLink(actorAssociationAKAMember, "Actor", akaActor);
            edr.SetLink(actorAssociationAKAMember, "Association", actorAssociation);
            edr.SetLink(actorAssociationAKAMember, "RoleSourceReference", dataSource.ActorAssociationRoleSourceReferences.Where(x => x.CodeValue == "aka").First());

            var actorAssociationMemberAKAKeyMap = new ActorAssociationMemberKeyMap
            {
                DataSource = dataSource,
                SourceKey = akaActorSourceKey
            };
            edr.AddToActorAssociationMemberKeyMaps(actorAssociationMemberAKAKeyMap);
            edr.SetLink(actorAssociationMemberAKAKeyMap, "Member", actorAssociationAKAMember);
            edr.SetLink(actorAssociationMemberAKAKeyMap, "DataSource", dataSource);
            #endregion
            edr.SaveChanges();
        }
        static void Select(string actorSourceKey)
        {
            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.Format.UseJson();
            edr.MergeOption = MergeOption.OverwriteChanges;

            var dataSource = edr.GetDataSource(_dataSourceCodeValue);//edr.GetDataSource(_dataSourceCodeValue);
            var actorKeyMap = edr.ActorKeyMaps.Expand("Actor").
                Where(x => x.DataSource.CodeValue == dataSource.CodeValue && x.SourceKey == actorSourceKey).SingleOrDefault();
            if (actorKeyMap == null)
            {
                Console.WriteLine("Select: ''.");
            }
            else
            {
                Person p = (Person)actorKeyMap.Actor;
                //Actor a = actorKeyMap.Actor;
                // ActorIdentity ai = edr.ActorIdentities.Where(x => x.Key == 3300).Single();
                // edr.AddLink(p, "Identities", ai);
                // edr.SaveChanges();
                //edr.UpdateObject(a);
                ActorEvent ae = edr.ActorEvents.Expand("Actor").Where(x => x.Actor.Key == p.Key).SingleOrDefault();
                ActorEventDate aed = edr.ActorEventDates.Expand("ActorEvent").Where(x => x.ActorEvent.Key == ae.Key && x.SourceReference.CodeValue =="DOB").SingleOrDefault();
                PersonName pn = edr.PersonNames.Expand("Person").Where(x => x.Person.Key == p.Key).SingleOrDefault();
                Demographic pd = edr.Demographics.Expand("Person").Where(x => x.Person.Key == p.Key).SingleOrDefault();
                Gender pg = edr.Genders.Expand("Person").Expand("SourceReference")
                    .Where(x => x.Person.Key == p.Key).SingleOrDefault();
                Race pr = edr.Races.Expand("Person").Expand("SourceReference")
                    .Where(x => x.Person.Key == p.Key).SingleOrDefault();
                Language lg = edr.Languages.Expand("Actor").Expand("SourceReference")
                    .Where(x => x.Actor.Key == p.Key).SingleOrDefault();
                Actor a = edr.Actors.Expand("Identities")
                    .Expand("Members/Association/Members/Actor")
                    .Expand("Members/Association/TypeSourceReference")
                    .Expand("Members/Association/Members/RoleSourceReference")
                    .Where(x => x.Key == p.Key && 
                        x.Members.Any(m => m.Association.Members.Any(maa => maa.RoleSourceReference.CodeValue == "aka")) &&
                        x.Members.Any(ma => ma.Association.TypeSourceReference.CodeValue == "AKA")).SingleOrDefault();
                Console.WriteLine("* * * * * * * * Select => SourceKey: '{0}'* * * * * * * * \r\nName: '{1} {2}'.", actorKeyMap.SourceKey, p.Name, pn.MiddleName);
                Console.WriteLine("DOB: {0}\r\nHeight: {1}\r\nWeight: {2}", aed.Date.ToString(), pd.Height.ToString(), pd.Weight.ToString());
                Console.WriteLine("Gender: {0}\r\nRace: {1}\r\nLanguage: {2}", pg.SourceReference.CodeValue, pr.SourceReference.CodeValue, lg.SourceReference.CodeValue);
                Console.WriteLine("DriverLicense: {0}   State:{1}\r\n", a.Identities[0].Value, a.Identities[0].AuthorizingOrganization);
                string akaName = "";
                foreach (var actor in a.Members[0].Association.Members)
                {
                    if (actor.RoleSourceReference.CodeValue == "aka")
                        akaName = actor.Actor.Name;
                }
                Console.WriteLine("AKA Person: {0}", akaName);
            }
        }

        static void Update(string actorSourceKey, string firstName, string lastName)
        {
            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.MergeOption = MergeOption.OverwriteChanges;

            var dataSource = edr.GetDataSource(_dataSourceCodeValue);
            var actorKeyMap = edr.ActorKeyMaps.Expand("Actor/Aoc.Edr.DataContext.Person/PersonNames").Where(x => x.DataSource.CodeValue == dataSource.CodeValue && x.SourceKey == actorSourceKey).Single();
            var person = (Person)actorKeyMap.Actor;
            var personName = person.PersonNames.Single();

            Console.WriteLine("Update: '{0}' to '{1}'.", string.Format("{0}", person.Name), string.Format("{0}, {1}", lastName, firstName));
            
            person.Name = string.Format("{0}, {1}", lastName, firstName);
            personName.FirstName = firstName;
            personName.LastName = lastName;
            edr.UpdateObject(person);
            edr.UpdateObject(personName);

            edr.SaveChanges(SaveChangesOptions.PatchOnUpdate, withSpinner: true);
        }

        static void Delete(string actorSourceKey)
        {
            var edr = new AocEdrDataServiceContext(_serviceUri);
            edr.MergeOption = MergeOption.OverwriteChanges;
            var dataSource = edr.GetDataSource(_dataSourceCodeValue);
            var actorKeyMap = edr.ActorKeyMaps.Expand("Actor/Aoc.Edr.DataContext.Person/PersonNames").Where(x => x.DataSource.CodeValue == dataSource.CodeValue && x.SourceKey == actorSourceKey).Single();
            var person = (Person)actorKeyMap.Actor;
            var personName = person.PersonNames.Single();
            Console.WriteLine("Delete: '{0}'.", actorKeyMap.Actor.Name);
            edr.DeleteObject(actorKeyMap);
            edr.DeleteObject(personName);
            edr.DeleteObject(actorKeyMap.Actor);

            edr.SaveChanges(withSpinner: true);
        }

        #region Initialize Reference Data
        static Program()
        {
            // Currently you can add standard references but your access will be removed after AOC defines their standards.
            try
            {
                InitializeEdrReferenceData();
                InitializeOrganizationReferenceData();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void InitializeOrganizationReferenceData()
        {
            var dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName); ;
            var personHairColorSourceReferencesCodeValues = new[] { "BLK", "BRN" };
            var personEyeColorSourceReferencesCodeValues = new[] { "BLK", "BRN" };
            var personGenderSourceReferencesCodeValues = new[] { "M", "F" };
            var personRaceSourceReferencesCodeValues = new[] { "W", "B", "A" };
            var languageTypeSourceReferencesCodeValues = new[] { "SPN", "ENG" };
            var actorIdentityTypeSourceReferencesCodeValues = new[] { "WA", "NY" };
            var actorAssociationRoleSourceReferencesCodeValues = new[] { "primary", "aka", "dba", "husband", "wife" };
            var actorAssociationTypeSourceReferencesCodeValues = new[] { "AKA", "DBA", "SPOUSE" };
            var actorEventDateSourceReferencesCodeValues = new[] {"DOB","DOD" };
            var context = new AocEdrDataServiceContext(_serviceUri);
            context.Format.UseJson();
            #region data source
            var dataSource = context.DataSources.Where(x => x.CodeValue == dataSourceCodeValue).SingleOrDefault();
            if (dataSource == null)
            {
                SecurityAccessLocator al = new SecurityAccessLocator
                {
                    UniformResourceIdentifier = "KCDC"
                };
                context.AddToSecurityAccessLocators(al);
                SecurityResourceOwner ro = new SecurityResourceOwner 
                { 
                    AccessLocator = al
                };
                context.AddToSecurityResourceOwners(ro);
                context.SetLink(ro, "AccessLocator", al);
                dataSource = new DataSource
                {
                    CodeValue = dataSourceCodeValue,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.MaxValue,
                    CodeDescription = "KCDC Test EDR",
                    ResourceOwner = ro
                };
                context.AddToDataSources(dataSource);
                context.SetLink(dataSource, "ResourceOwner", ro);
                context.SaveChanges();
            }
            #endregion

            #region person hair color source references
            var references = context.HairColorSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in personHairColorSourceReferencesCodeValues)
            {
                var reference = references.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Hair Color Description"
                    };
                    context.AddToHairColorSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.HairColorStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person Eye color source references
            var references1 = context.EyeColorSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in personEyeColorSourceReferencesCodeValues)
            {
                var reference = references1.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new EyeColorSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Eye Color Description"
                    };
                    context.AddToEyeColorSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.EyeColorStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person Gender source references
            var references2 = context.GenderSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in personGenderSourceReferencesCodeValues)
            {
                var reference = references2.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new GenderSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Gender Description"
                    };
                    context.AddToGenderSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.GenderStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person Race source references
            var references3 = context.RaceSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in personRaceSourceReferencesCodeValues)
            {
                var reference = references3.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new RaceSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Race Description"
                    };
                    context.AddToRaceSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.RaceStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person language Type source references
            var references4 = context.LanguageSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in languageTypeSourceReferencesCodeValues)
            {
                var reference = references4.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new LanguageSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "language Type Description"
                    };
                    context.AddToLanguageSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.LanguageStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person ActorIdentity Type source references
            var references5 = context.ActorIdentitySourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in actorIdentityTypeSourceReferencesCodeValues)
            {
                var reference = references5.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorIdentitySourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "ActorIdentity Type Description"
                    };
                    context.AddToActorIdentitySourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.ActorIdentityStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges(SaveChangesOptions.Batch);
                }
            }
            #endregion
            #region person Actor Association Role source references
            var references6 = context.ActorAssociationRoleSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in actorAssociationRoleSourceReferencesCodeValues)
            {
                var reference = references6.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorAssociationRoleSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Association Role Description"
                    };
                    context.AddToActorAssociationRoleSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "RoleStandardReference", context.ActorAssociationRoleStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges();
                }
            }
            #endregion
            #region person Actor Association Type source references
            var references7 = context.ActorAssociationTypeSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in actorAssociationTypeSourceReferencesCodeValues)
            {
                var reference = references7.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorAssociationTypeSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Association Type Description"
                    };
                    context.AddToActorAssociationTypeSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "TypeStandardReference", context.ActorAssociationTypeStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges();
                }
            }
            #endregion
            #region person Actor event date source references
            var references8 = context.ActorEventDateSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue);
            foreach (var codeValue in actorEventDateSourceReferencesCodeValues)
            {
                var reference = references8.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorEventDateSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Association Type Description"
                    };
                    context.AddToActorEventDateSourceReferences(reference);
                    context.SetLink(reference, "DataSource", dataSource);
                    context.SetLink(reference, "StandardReference", context.ActorEventDateStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).FirstOrDefault());
                    context.SaveChanges();
                }
            }
            #endregion

        }

        static void InitializeEdrReferenceData()
        {
            var personHairColorStandardReferenceCodeValues = new[] { "EDR-BLK", "EDR-BRN" };
            var personEyeColorStandardReferenceCodeValues = new[] { "EDR-BLK", "EDR-BRN" };
            var personGenderStandardReferenceCodeValues = new[] { "EDR-M", "EDR-F" };
            var personRaceStandardReferenceCodeValues = new[] { "EDR-W", "EDR-B", "EDR-A" };
            var languageTypeStandardReferenceCodeValues = new[] { "EDR-SPN", "EDR-ENG" };
            var actorIdentityTypeStandardReferenceCodeValues = new[] { "EDR-WA", "EDR-NY" };
            var actorAssociationRoleStandardReferenceCodeValues = new[] { "EDR-primary", "EDR-aka", "EDR-dba", "EDR-husband", "EDR-wife" };
            var actorAssociationTypeStandardReferenceCodeValues = new[] { "EDR-AKA", "EDR-DBA", "EDR-SPOUSE" };
            var actorEventDateStandardReferenceCodeValues = new[] {"EDR-DOB","EDR-DOD" };
            #region person hair color standard references
            var context = new AocEdrDataServiceContext(_serviceUri);
            context.Format.UseJson();
            foreach (var codeValue in personHairColorStandardReferenceCodeValues)
            {
                var reference = context.HairColorStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Hair Color Description",
                    };
                    context.AddToHairColorStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region person eye color standard references
            foreach (var codeValue in personEyeColorStandardReferenceCodeValues)
            {
                var reference = context.EyeColorStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new EyeColorStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Eye Color Description",
                    };
                    context.AddToEyeColorStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region person Gender standard references
            foreach (var codeValue in personGenderStandardReferenceCodeValues)
            {
                var reference = context.GenderStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new GenderStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Gender Description",
                    };
                    context.AddToGenderStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region person Race standard references
            foreach (var codeValue in personRaceStandardReferenceCodeValues)
            {
                var reference = context.RaceStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new RaceStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Race Description",
                    };
                    context.AddToRaceStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region language type standard references
            foreach (var codeValue in languageTypeStandardReferenceCodeValues)
            {
                var reference = context.LanguageStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new LanguageStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Language Description",
                    };
                    context.AddToLanguageStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region actor identity standard references
            var securityCode = context.SecurityCodes.Where(x => x.CodeValue == "KCDC").FirstOrDefault();
            if (securityCode == null)
            {
                securityCode = new SecurityCode
                {
                    CodeValue = "KCDC",
                    CodeDescription = "KCDC Security Code"
                };
                context.AddToSecurityCodes(securityCode);
            }
            
            foreach (var codeValue in actorIdentityTypeStandardReferenceCodeValues)
            {
                var reference = context.ActorIdentityStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorIdentityStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "DOL Description",
                        Code = securityCode
                    };
                    context.AddToActorIdentityStandardReferences(reference);
                    context.SetLink(reference, "Code", securityCode);
                    context.SaveChanges();
                }
            }
            #endregion
            #region actor association role standard references
            foreach (var codeValue in actorAssociationRoleStandardReferenceCodeValues)
            {
                var reference = context.ActorAssociationRoleStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorAssociationRoleStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Association Role Description",
                    };
                    context.AddToActorAssociationRoleStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region actor association type standard references
            foreach (var codeValue in actorAssociationTypeStandardReferenceCodeValues)
            {
                var reference = context.ActorAssociationTypeStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorAssociationTypeStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Association Type Description",
                    };
                    context.AddToActorAssociationTypeStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
            #region actor event date standard references
            foreach (var codeValue in actorEventDateStandardReferenceCodeValues)
            {
                var reference = context.ActorEventDateStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new ActorEventDateStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "Actor Event Date Description"
                    };
                    context.AddToActorEventDateStandardReferences(reference);
                    context.SaveChanges();
                }
            }
            #endregion
        }
        #endregion
    }
}
