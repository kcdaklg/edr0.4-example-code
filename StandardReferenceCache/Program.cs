﻿using EdrServiceClient.gov.wa.courts.api.edr;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StandardReferenceCache
{
    /// <summary>
    /// This example demonstrates that reference data can be cached locally and
    /// used during EDR exchanges. Caching reference data allows applications
    /// to significantly reduce the overall communication between the source
    /// system and EDR.
    /// </summary>
    class Program
    {
        static readonly Uri _serviceUri = new Uri("https://api-qa.courts.wa.gov/EdrDataServices/AocEdrDataService.svc/");
        static readonly string _dataSourceCodeValue = Util.DataSourceCodeValue(typeof(Program).FullName);
        static readonly Random _random = new Random();

        static void Main(string[] args)
        {

            #region Ignore (Temporary!!!)
            Util.Spinner("Initialize", () =>
            {
                MockReferenceData();
            });
            #endregion

            do
            {
                var edr = new AocEdrDataServiceContext(_serviceUri);
                // Capture the total number of service invocations.
                var serviceInvocationCount = 0;

                // For each service call, increment service invocation count.
                edr.SendingRequest2 += (s, e) => serviceInvocationCount++;

                // Extract reference data from cache. If no reference data is found
                // in the cache then pull the reference data from the service,
                // cache the results, and return the reference data.
                var dataSource = Cache<DataSource>(edr,
                    "DataSources",
                    x => x.CodeValue == _dataSourceCodeValue).Single();
                var personHairColorSourceReferences = Cache<HairColorSourceReference>(edr,
                    "HairColorSourceReferences",
                    x => x.DataSource.CodeValue == _dataSourceCodeValue);

                // Add a person to EDR using the cache.
                Add_A_Person_To_EDR_Using_The_Cache(edr, dataSource, personHairColorSourceReferences);

                // During the first run of this example you will receive a service
                // invocation count of 6. After the second run you will receive an
                // invocation count of 4. The second run contains fewer records
                // because no reference data was pulled from the service. Instead,
                //the reference data was pulled from cache.
                Console.WriteLine("Service Invocation Count: {0}", serviceInvocationCount);
                Console.WriteLine("To continue press:\n\t'R' = Run using cache.\n\t'D' = Delete cache and run.\n\tAny other key to exit.");
                // Keep console open.
                var key = Console.ReadKey();
                Console.Clear();
                switch (key.KeyChar)
                {
                    case 'r':
                    case 'R':
                        continue;
                    case 'd':
                    case 'D':
                        var dir = new DirectoryInfo(".");
                        foreach (var file in dir.EnumerateFiles(string.Format("cache-{0}-*.txt", _dataSourceCodeValue)))
                        {
                            file.Delete();
                        }
                        continue;
                    default:
                        return;
                }
            } while (true);
        }

        /// <summary>
        /// This method will return cached entities for a given entity set. If
        /// no cache entities are found for the given entity set name then the
        /// EDR is queried and the response is cached and returned.
        /// </summary>
        /// <typeparam name="T">The type returned by the query</typeparam>
        /// <param name="context">The data service context containing the entity set to be cached.</param>
        /// <param name="entitySetName">The name of the set that contains the resource.</param>
        /// <param name="where">A function to test each element for a condition.</param>
        /// <returns>An System.Linq.IQueryable<T> that contains elements from the input sequence</returns>
        static IEnumerable<T> Cache<T>(DataServiceContext context, string entitySetName, Expression<Func<T, bool>> where)
        {
            IEnumerable<T> entities;
            var cache = string.Format("cache-{0}-{1}.txt", _dataSourceCodeValue, entitySetName);
            // look for cache file (or database/NoSQL/etc...)
            if (File.Exists(cache))
            {
                entities = context.Entities.Where(x => x.Entity is T).Select(x => x.Entity).Cast<T>();
                // If the entities have not yet been attached to the data
                // service context then de-serialize and attach.
                if (entities.Count() == 0)
                {
                    Console.WriteLine("Reading '{0}' from cache.", entitySetName);
                    var json = File.ReadAllText(cache);
                    entities = JsonConvert.DeserializeObject<IEnumerable<T>>(json);
                    foreach (var hcsr in entities)
                    {
                        // you must re-attach the reference data to the data
                        // service context.
                        context.AttachTo(entitySetName, hcsr);
                    }
                }
                if (entities.Count() != 0)
                {
                    return entities;
                }
            }
            // Otherwise, query EDR by entity set name, then serialize elements
            // to file system (or database/NoSQL/etc...).
            {
                entities = Util.Spinner(string.Format("Query {0}", entitySetName), () =>
                {
                    return context.CreateQuery<T>(entitySetName).Where(where).ToArray();
                });
                Console.WriteLine("Writing '{0}' to cache.", entitySetName);
                var json = JsonConvert.SerializeObject(entities, Formatting.Indented);
                File.WriteAllText(cache, json);
            }
            return entities;
        }

        /// <summary>
        /// Basic insert using cache data source and person hair color source
        /// references. This method is only for the purposes of demonstrating
        /// that the cached reference data can be used.
        /// </summary>
        /// <param name="dataSource">Cached Data Source</param>
        /// <param name="personHairColorSourceReferences">Cached Person Hair Color Source References</param>
        static void Add_A_Person_To_EDR_Using_The_Cache(AocEdrDataServiceContext edr, DataSource dataSource, IEnumerable<HairColorSourceReference> personHairColorSourceReferences)
        {
            #region Create person object graph.
            var person = new Person
            {
                DataSource = dataSource,
                KeyMaps =
                {
                    new ActorKeyMap
                    {
                        DataSource = dataSource,
                        SourceKey = _random.Next().ToString()
                    }
                },
                Name = string.Format("{0}, {1}", _random.Next(), _random.Next()),
                HairColors =
                {
                    new  HairColor
                    {
                        DataSource = dataSource,
                        SourceReference = personHairColorSourceReferences.Where(x => x.CodeValue == "BLK").First()
                    }
                }
            };
            #endregion

            #region Save Person
            #region Add entity to data service context and set links between entities.
            edr.AddToActors(person);
            edr.SetLink(person, "DataSource", person.DataSource);
            foreach (var akm in person.KeyMaps)
            {
                edr.AddToActorKeyMaps(akm);
                edr.SetLink(akm, "DataSource", akm.DataSource);
            }
            foreach (var phc in person.HairColors)
            {
                edr.AddToHairColors(phc);
                edr.SetLink(phc, "DataSource", phc.DataSource);
                edr.SetLink(phc, "Person", person);
                edr.SetLink(phc, "SourceReference", phc.SourceReference);
            }
            #endregion

            #region Ignore (security constraint)
            // In the near future, the SecurityResourceOwner field will be populated by the
            // EDR services.
            edr.SetLink(person, "ResourceOwner", edr.GetSecurityResourceOwner(dataSource));
            #endregion

            #region Save Changes (Adds Person, Key Map, Hair Color, and Linking)
            edr.SaveChanges(withSpinner: true);
            #endregion
            #endregion
        }

        #region Mock Reference Data

        /// <summary>
        /// This method will insert a data source, hair color source reference,
        /// and hair color standard reference mock data. This code is only
        /// needed for the demo. In a production environment, all of these
        /// values would already be in place.
        /// </summary>
        static void MockReferenceData()
        {
            AocEdrDataServiceContext edr = new AocEdrDataServiceContext(_serviceUri);

            #region data source
            var dataSource = edr.GetDataSource(_dataSourceCodeValue);
            #endregion

            #region person hair color source references
            var codeValues = new[] { "BLK", "BRN" };
            // Return all person hair color source references
            var personHairColorSourceReferences = edr.HairColorSourceReferences.Where(x => x.DataSource.CodeValue == dataSource.CodeValue).ToArray();
            foreach (var codeValue in codeValues)
            {
                var reference = personHairColorSourceReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorSourceReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "A Description"
                    };
                    edr.AddToHairColorSourceReferences(reference);
                    edr.SetLink(reference, "DataSource", dataSource);
                    var sr = edr.HairColorStandardReferences.Where(x => x.CodeValue == "EDR-" + codeValue).First();
                    edr.SetLink(reference, "StandardReference", sr);
                    edr.SaveChanges();
                }
            }
            #endregion

            #region person hair color standard references
            var edrCodeValues = new[] { "EDR-BLK", "EDR-BRN" };
            // Only have the service return values stored in the code values
            // array.
            var personHairColorStandardReferences = edr.HairColorStandardReferences.ToArray();
            foreach (var codeValue in edrCodeValues)
            {
                var reference = personHairColorStandardReferences.Where(x => x.CodeValue == codeValue).FirstOrDefault();
                if (reference == null)
                {
                    reference = new HairColorStandardReference
                    {
                        CodeValue = codeValue,
                        CodeDescription = "A Description",
                    };
                    edr.AddToHairColorStandardReferences(reference);
                    edr.SaveChanges();
                }
            }
            #endregion
        }

        #endregion
    }
}
