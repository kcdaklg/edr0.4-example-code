﻿using EdrServiceClient.gov.wa.courts.api.edr;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Util
{

    /// <summary>
    /// The data source code value is used when creating and selecting an
    /// existing data source. In our examples we are using a combination of
    /// user identity name and class full name. This is simply for
    /// isolation between other users working with the example code and
    /// isolation between each sample. In normal situations you may wish to
    /// have several systems uses the same data source and in those cases
    /// you would use the same data source code value.
    /// </summary>
    /// <param name="suffix">Allows the data source code value to be further scoped.</param>
    /// <returns></returns>
    public static string DataSourceCodeValue(params string[] suffix)
    {
        string value = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        if (suffix != null && suffix.Length > 0)
        {
            value += "." + string.Join(".", suffix);
        }
        // this will shrink the code value into something smaller than 60 char.
        string hash = string.Join("", System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(value)).Select(s => s.ToString("x2")));
        return hash;
    }

    /// <summary>
    /// Helper method for retrieving a data source. If no data source is found then we create a new data source.
    /// </summary>
    /// <param name="copy">EDR Data Service Context</param>
    /// <param name="dataSourceCodeValue">Data Source Code Value</param>
    /// <returns></returns>
    public static DataSource GetDataSource(this AocEdrDataServiceContext edr, string dataSourceCodeValue, bool withSpinner = false)
    {
        Func<DataSource> func = () =>
        {
            // check context.
            var dataSource = edr.Entities.Select(x => x.Entity as DataSource).Where(x => x != null && x.CodeValue == dataSourceCodeValue).FirstOrDefault();
            // if true, check the EDR.
            if (dataSource == null)
            {
                dataSource = edr.DataSources.Where(x => x.CodeValue == dataSourceCodeValue).FirstOrDefault();
            }
            // If true, add to EDR
            if (dataSource == null)
            {
                // create a copy so that we don't save changes to the data context of the calling method.
                var copy = new AocEdrDataServiceContext(edr.BaseUri);

                // Create security access locator
                SecurityAccessLocator al = new SecurityAccessLocator()
                {
                    UniformResourceIdentifier = Guid.NewGuid().ToString()
                };
                copy.AddToSecurityAccessLocators(al);

                // Create security resource owner
                var o = new SecurityResourceOwner()
                {
                };
                copy.AddToSecurityResourceOwners(o);
                copy.SetLink(o, "AccessLocator", al);

                //copy.SaveChanges();

                // Create data source
                var ds = new DataSource
                {
                    CodeValue = dataSourceCodeValue,
                    CodeDescription = "Some Code Description"
                };
                copy.AddToDataSources(ds);
                copy.SetLink(ds, "ResourceOwner", o);

                // Save changes
                copy.SaveChanges();

                // now, attach to data context so that it can be used.
                var securityResourceOwner = new SecurityResourceOwner { Key = o.Key };
                edr.AttachTo("SecurityResourceOwners", securityResourceOwner);
                dataSource = new DataSource { Key = ds.Key, CodeValue = ds.CodeValue };
                edr.AttachTo("DataSources", dataSource);
            }
            return dataSource;
        };
        return withSpinner ? Util.Spinner("GetDataSource", func) : func();
    }

    /// <summary>
    /// This method is a temporary helper for creating and retrieving an
    /// SecurityResourceOwner entity. In the future, the SecurityResourceOwner entity will be populated
    /// by the EDR.
    /// </summary>
    /// <param name="copy">EDR Data Service Context</param>
    /// <param name="dataSource">Data Source</param>
    /// <returns></returns>
    public static SecurityResourceOwner GetSecurityResourceOwner(this AocEdrDataServiceContext edr, DataSource dataSource, bool withSpinner = false)
    {
        Func<SecurityResourceOwner> func = () =>
        {
            // check context.
            var SecurityResourceOwner = edr.Entities.Select(x => x.Entity as SecurityResourceOwner).Where(x => x != null).FirstOrDefault();
            // if true, check the EDR.
            if (SecurityResourceOwner == null)
            {
                SecurityResourceOwner = edr.SecurityResourceOwners.Where(x => x.DataSources.Any(y => y.CodeValue == dataSource.CodeValue)).First();
            }
            return SecurityResourceOwner;
        };

        return withSpinner ? Util.Spinner("GetSecurityResourceOwner", func) : func();
    }

    /// <summary>
    /// Saves the changes that the System.Data.Services.Client.DataServiceContext is tracking to storage.
    /// </summary>
    /// <param name="edr">EDR Data Service Context</param>
    /// <param name="withSpinner">if true, show spinner</param>
    /// <param name="options">A member of the System.Data.Services.Client.MergeOption enumeration that specifies the materialization option.</param>
    /// <returns>A System.Data.Services.Client.DataServiceResponse that contains status, headers, and errors that result from the call to System.Data.Services.Client.DataServiceContext.SaveChanges.Remarks.</returns>
    public static DataServiceResponse SaveChanges(this AocEdrDataServiceContext edr, bool withSpinner)
    {
        return edr.SaveChanges(SaveChangesOptions.None, withSpinner);
    }

    /// <summary>
    /// Saves the changes that the System.Data.Services.Client.DataServiceContext is tracking to storage.
    /// </summary>
    /// <param name="edr">EDR Data Service Context</param>
    /// <param name="withSpinner">if true, show spinner</param>
    /// <param name="options">A member of the System.Data.Services.Client.MergeOption enumeration that specifies the materialization option.</param>
    /// <returns>A System.Data.Services.Client.DataServiceResponse that contains status, headers, and errors that result from the call to System.Data.Services.Client.DataServiceContext.SaveChanges.Remarks.</returns>
    public static DataServiceResponse SaveChanges(this AocEdrDataServiceContext edr, SaveChangesOptions options, bool withSpinner)
    {
        Func<DataServiceResponse> func = () => edr.SaveChanges();
        return withSpinner ? Util.Spinner("SaveChanges", func) : func();
    }

    public static void Spinner(string title, Action action)
    {
        Spinner<object>(title, () =>
        {
            action();
            return null;
        });
    }

    public static T Spinner<T>(string title, Func<T> func)
    {
        Task<T> t = SpinnerAsync<T>(title, () =>
        {
            return Task.Run(func);
        });
        t.Wait();
        return t.Result;
    }

    public static async Task SpinnerAsync(string title, Func<Task> func)
    {
        await SpinnerAsync<object>(title, async () =>
        {
            await func();
            return null;
        });
    }

    public static async Task<T> SpinnerAsync<T>(string title, Func<Task<T>> func)
    {
        Console.Write("{0} ", title);
        var task = func();
        for (int i = 0; !task.IsCompleted; i++)
        {
            switch (i % 4)
            {
                case 0: Console.Write("/"); break;
                case 1: Console.Write("-"); break;
                case 2: Console.Write("\\"); break;
                case 3: Console.Write("|"); break;
            }
            await Task.Delay(50);
            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        }
        Console.WriteLine(" ");
        return task.Result;
    }
}