﻿using System;
using System.Threading.Tasks;
using System.Data.Services.Client;
using System.Threading;
using System.Collections.Concurrent;

public class ObjectPool<T>
{
    private readonly Func<T> _objectGenerator;
    private readonly ConcurrentBag<T> _poolItems;
    private readonly SemaphoreSlim _semaphore;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="concurrency">The number objects in the pool that can be used concurrently.</param>
    /// <param name="objectGenerator">The delegate used when creating an object for the pool.</param>
    public ObjectPool(int concurrency, Func<T> objectGenerator)
    {
        _objectGenerator = objectGenerator;
        _poolItems = new ConcurrentBag<T>();
        _semaphore = new SemaphoreSlim(concurrency);
    }

    public T[] ToArray()
    {
        return _poolItems.ToArray();
    }

    public async Task Invoke(Func<T, Task> work)
    {
        await Invoke<object>(async pi =>
        {
            await work(pi);
            return null;
        });
    }

    public T GetObject()
    {
        T item;
        if (_poolItems.TryTake(out item)) return item;
        item = _objectGenerator();
        return item;
    }

    public void PutObject(T item)
    {
        _poolItems.Add(item);
    }

    public async Task<TResult> Invoke<TResult>(Func<T, Task<TResult>> work)
    {
        await _semaphore.WaitAsync();
        T poolItem = GetObject();
        try
        {
            return await work(poolItem);
        }
        finally
        {
            PutObject(poolItem);
            _semaphore.Release();
        }
    }
}
